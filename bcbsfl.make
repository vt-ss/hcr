; This file will build the entire bcbsfl.com site directory
; Run it as ...
; drush make --no-core --contrib-destination=. bcbsfl.make .

core = 7.x
api = 2

projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][subdir] = "contrib"

projects[admin_views][version] = "1.2"
projects[admin_views][subdir] = "contrib"

projects[advagg][version] = "2.6"
projects[advagg][subdir] = "contrib"

projects[breadcrumbs_by_path][version] = "1.0-alpha11"
projects[breadcrumbs_by_path][subdir] = "contrib"

projects[coffee][version] = "2.2"
projects[coffee][subdir] = "contrib"

projects[ctools][version] = "1.4"
projects[ctools][subdir] = "contrib"

projects[date][version] = "2.7"
projects[date][subdir] = "contrib"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

projects[devel][version] = "1.5"
projects[devel][subdir] = "contrib"

projects[entity][version] = "1.5"
projects[entity][subdir] = "contrib"

projects[entitycache][version] = "1.2"
projects[entitycache][subdir] = "contrib"

projects[entityreference][version] = "1.1"
projects[entityreference][subdir] = "contrib"

projects[features][version] = "2.0"
projects[features][subdir] = "contrib"

projects[field_group][version] = "1.3"
projects[field_group][subdir] = "contrib"

projects[file_entity][version] = "2.0-alpha3"
projects[file_entity][subdir] = "contrib"

projects[jquery_update][version] = "2.4"
projects[jquery_update][subdir] = "contrib"

projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

projects[link][version] = "1.2"
projects[link][subdir] = "contrib"

projects[linkit][version] = "3.1"
projects[linkit][subdir] = "contrib"

projects[media][version] = "1.4"
projects[media][subdir] = "contrib"

projects[media_youtube][version] = "2.0-rc4"
projects[media_youtube][subdir] = "contrib"

projects[menu_block][version] = "2.4"
projects[menu_block][subdir] = "contrib"

projects[menu_admin_per_menu][version] = "1.0"
projects[menu_admin_per_menu][subdir] = "contrib"

projects[module_filter][version] = "2.0-alpha2"
projects[module_filter][subdir] = "contrib"

projects[multiform][version] = "1.0"
projects[multiform][subdir] = "contrib"

projects[panels][version] = "3.4"
projects[panels][subdir] = "contrib"

projects[plupload][version] = "1.6"
projects[plupload][subdir] = "contrib"

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[redirect][version] = "1.0-rc1"
projects[redirect][subdir] = "contrib"

projects[scheduler][version] = "1.2"
projects[scheduler][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[views][version] = "3.7"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.2"
projects[views_bulk_operations][subdir] = "contrib"

projects[wysiwyg][version] = "2.2"
projects[wysiwyg][subdir] = "contrib"

projects[xmlsitemap][version] = "2.0"
projects[xmlsitemap][subdir] = "contrib"

projects[zurb-foundation][version] = "4.0"

projects[ds][version] = "2.6"
projects[ds][subdir] = "contrib"

projects[accordion_menu][version] = "1.2"
projects[accordion_menu][subdir] = "contrib"

