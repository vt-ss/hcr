<?php
/**
 * @file
 * hcr_config.ds.inc
 */

/**
 * Implements hook_ds_custom_fields_info().
 */
function hcr_config_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'hcr_right_side';
  $ds_field->label = 'HCR Right Side';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'devel|execute_php',
    'block_render' => '3',
  );
  $export['hcr_right_side'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function hcr_config_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|hcr_article|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'hcr_article';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'body',
        1 => 'field_tags',
      ),
      'hcr_image' => array(
        2 => 'field_image',
      ),
    ),
    'fields' => array(
      'body' => 'ds_content',
      'field_tags' => 'ds_content',
      'field_image' => 'hcr_image',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|hcr_article|full'] = $ds_layout;

  return $export;
}
