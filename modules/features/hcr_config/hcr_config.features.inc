<?php
/**
 * @file
 * hcr_config.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hcr_config_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function hcr_config_node_info() {
  $items = array(
    'hcr_article' => array(
      'name' => t('HCR Article'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
