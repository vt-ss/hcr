<?php
/**
 * @file
 * hcr_config.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function hcr_config_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-hcr-help.
  $menus['menu-hcr-help'] = array(
    'menu_name' => 'menu-hcr-help',
    'title' => 'HCR Help',
    'description' => 'Health Care Reform side links',
  );
  // Exported menu: menu-hcr-topics.
  $menus['menu-hcr-topics'] = array(
    'menu_name' => 'menu-hcr-topics',
    'title' => 'HCR Topics',
    'description' => 'Health Care Reform Topics. Left sidebar',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('HCR Help');
  t('HCR Topics');
  t('Health Care Reform Topics. Left sidebar');
  t('Health Care Reform side links');
  t('Main menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');


  return $menus;
}
