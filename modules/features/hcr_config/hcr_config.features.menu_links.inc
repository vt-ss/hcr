<?php
/**
 * @file
 * hcr_config.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function hcr_config_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_about-us:<front>
  $menu_links['main-menu_about-us:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'About us',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_about-us:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: main-menu_find-a-center:<front>
  $menu_links['main-menu_find-a-center:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Find a Center',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_find-a-center:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: main-menu_find-a-doctor:<front>
  $menu_links['main-menu_find-a-doctor:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Find a Doctor',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_find-a-doctor:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_find-an-agent:<front>
  $menu_links['main-menu_find-an-agent:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Find an Agent',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_find-an-agent:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_home:<front>
  $menu_links['main-menu_home:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(
      'identifier' => 'main-menu_home:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_shop-our-plans:<front>
  $menu_links['main-menu_shop-our-plans:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Shop our Plans',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_shop-our-plans:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-hcr-help_questions--answers:<front>
  $menu_links['menu-hcr-help_questions--answers:<front>'] = array(
    'menu_name' => 'menu-hcr-help',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Questions & Answers',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-hcr-help_questions--answers:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-hcr-help_resources:<front>
  $menu_links['menu-hcr-help_resources:<front>'] = array(
    'menu_name' => 'menu-hcr-help',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Resources',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-hcr-help_resources:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-hcr-help_subsidy-estimator:<front>
  $menu_links['menu-hcr-help_subsidy-estimator:<front>'] = array(
    'menu_name' => 'menu-hcr-help',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Subsidy Estimator',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-hcr-help_subsidy-estimator:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-hcr-help_timeline:<front>
  $menu_links['menu-hcr-help_timeline:<front>'] = array(
    'menu_name' => 'menu-hcr-help',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Timeline',
    'options' => array(
      'attributes' => array(
        'title' => '

',
      ),
      'identifier' => 'menu-hcr-help_timeline:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-hcr-topics_are-there-different-kinds-of-plans:node/3
  $menu_links['menu-hcr-topics_are-there-different-kinds-of-plans:node/3'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/3',
    'router_path' => 'node/%',
    'link_title' => 'Are there different kinds of plans?',
    'options' => array(
      'identifier' => 'menu-hcr-topics_are-there-different-kinds-of-plans:node/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-hcr-topics_what-is-health-care-reform:<front>',
  );
  // Exported menu link: menu-hcr-topics_for-agents:node/10
  $menu_links['menu-hcr-topics_for-agents:node/10'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/10',
    'router_path' => 'node/%',
    'link_title' => 'For agents',
    'options' => array(
      'identifier' => 'menu-hcr-topics_for-agents:node/10',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'menu-hcr-topics_resources-for--professionals:<front>',
  );
  // Exported menu link: menu-hcr-topics_for-employers:node/12
  $menu_links['menu-hcr-topics_for-employers:node/12'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/12',
    'router_path' => 'node/%',
    'link_title' => 'For employers',
    'options' => array(
      'identifier' => 'menu-hcr-topics_for-employers:node/12',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'menu-hcr-topics_resources-for--professionals:<front>',
  );
  // Exported menu link: menu-hcr-topics_for-providers:node/13
  $menu_links['menu-hcr-topics_for-providers:node/13'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/13',
    'router_path' => 'node/%',
    'link_title' => 'For providers',
    'options' => array(
      'identifier' => 'menu-hcr-topics_for-providers:node/13',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'menu-hcr-topics_resources-for--professionals:<front>',
  );
  // Exported menu link: menu-hcr-topics_how-can-i-get--insured:<front>
  $menu_links['menu-hcr-topics_how-can-i-get--insured:<front>'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'How can I get  insured?',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-hcr-topics_how-can-i-get--insured:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-hcr-topics_how-can-i-get-help-paying-for-insurance:node/11
  $menu_links['menu-hcr-topics_how-can-i-get-help-paying-for-insurance:node/11'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/11',
    'router_path' => 'node/%',
    'link_title' => 'How can I get help paying for insurance?',
    'options' => array(
      'identifier' => 'menu-hcr-topics_how-can-i-get-help-paying-for-insurance:node/11',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-hcr-topics_how-can-i-get--insured:<front>',
  );
  // Exported menu link: menu-hcr-topics_how-has-health-care-reform-changed-things:node/1
  $menu_links['menu-hcr-topics_how-has-health-care-reform-changed-things:node/1'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/1',
    'router_path' => 'node/%',
    'link_title' => 'How has health care reform changed things?',
    'options' => array(
      'identifier' => 'menu-hcr-topics_how-has-health-care-reform-changed-things:node/1',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-hcr-topics_what-is-health-care-reform:<front>',
  );
  // Exported menu link: menu-hcr-topics_i-dont-have-insurance:node/4
  $menu_links['menu-hcr-topics_i-dont-have-insurance:node/4'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/4',
    'router_path' => 'node/%',
    'link_title' => 'I don\'t have insurance',
    'options' => array(
      'identifier' => 'menu-hcr-topics_i-dont-have-insurance:node/4',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-hcr-topics_where-do-i-fit-in:<front>',
  );
  // Exported menu link: menu-hcr-topics_i-have-insurance:node/5
  $menu_links['menu-hcr-topics_i-have-insurance:node/5'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/5',
    'router_path' => 'node/%',
    'link_title' => 'I have insurance',
    'options' => array(
      'identifier' => 'menu-hcr-topics_i-have-insurance:node/5',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-hcr-topics_where-do-i-fit-in:<front>',
  );
  // Exported menu link: menu-hcr-topics_i-have-medicaid:node/6
  $menu_links['menu-hcr-topics_i-have-medicaid:node/6'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/6',
    'router_path' => 'node/%',
    'link_title' => 'I have Medicaid',
    'options' => array(
      'identifier' => 'menu-hcr-topics_i-have-medicaid:node/6',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-hcr-topics_where-do-i-fit-in:<front>',
  );
  // Exported menu link: menu-hcr-topics_i-have-medicare:node/7
  $menu_links['menu-hcr-topics_i-have-medicare:node/7'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/7',
    'router_path' => 'node/%',
    'link_title' => 'I have Medicare',
    'options' => array(
      'identifier' => 'menu-hcr-topics_i-have-medicare:node/7',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-hcr-topics_where-do-i-fit-in:<front>',
  );
  // Exported menu link: menu-hcr-topics_resources-for--professionals:<front>
  $menu_links['menu-hcr-topics_resources-for--professionals:<front>'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Resources for  professionals',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-hcr-topics_resources-for--professionals:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-hcr-topics_what-if-i-have-questions:node/9
  $menu_links['menu-hcr-topics_what-if-i-have-questions:node/9'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/9',
    'router_path' => 'node/%',
    'link_title' => 'What if I have questions?',
    'options' => array(
      'identifier' => 'menu-hcr-topics_what-if-i-have-questions:node/9',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-hcr-topics_how-can-i-get--insured:<front>',
  );
  // Exported menu link: menu-hcr-topics_what-is-health-care-reform:<front>
  $menu_links['menu-hcr-topics_what-is-health-care-reform:<front>'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'What is health care reform?',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-hcr-topics_what-is-health-care-reform:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-hcr-topics_what-is-health-insurance:node/2
  $menu_links['menu-hcr-topics_what-is-health-insurance:node/2'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/2',
    'router_path' => 'node/%',
    'link_title' => 'What is health insurance?',
    'options' => array(
      'identifier' => 'menu-hcr-topics_what-is-health-insurance:node/2',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-hcr-topics_what-is-health-care-reform:<front>',
  );
  // Exported menu link: menu-hcr-topics_where-can-i-buy-insurance:node/8
  $menu_links['menu-hcr-topics_where-can-i-buy-insurance:node/8'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => 'node/8',
    'router_path' => 'node/%',
    'link_title' => 'Where can I buy insurance?',
    'options' => array(
      'identifier' => 'menu-hcr-topics_where-can-i-buy-insurance:node/8',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-hcr-topics_how-can-i-get--insured:<front>',
  );
  // Exported menu link: menu-hcr-topics_where-do-i-fit-in:<front>
  $menu_links['menu-hcr-topics_where-do-i-fit-in:<front>'] = array(
    'menu_name' => 'menu-hcr-topics',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Where do I fit in?',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-hcr-topics_where-do-i-fit-in:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About us');
  t('Are there different kinds of plans?');
  t('Find a Center');
  t('Find a Doctor');
  t('Find an Agent');
  t('For agents');
  t('For employers');
  t('For providers');
  t('Home');
  t('How can I get  insured?');
  t('How can I get help paying for insurance?');
  t('How has health care reform changed things?');
  t('I don\'t have insurance');
  t('I have Medicaid');
  t('I have Medicare');
  t('I have insurance');
  t('Questions & Answers');
  t('Resources');
  t('Resources for  professionals');
  t('Shop our Plans');
  t('Subsidy Estimator');
  t('Timeline');
  t('What if I have questions?');
  t('What is health care reform?');
  t('What is health insurance?');
  t('Where can I buy insurance?');
  t('Where do I fit in?');


  return $menu_links;
}
