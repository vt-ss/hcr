#!/bin/bash

if [ ! -d modules/contrib ]; then
  chmod +w .
  drush make --no-core --contrib-destination=. bcbsfl.make .
  chmod -w .
fi

drush site-install --site-mail=vt@spinspire.com --account-mail=vt@spinspire.com --account-pass=deadbeef99

#drush -y dis-bad
drush pm-disable -y update,overlay,dashboard,comment,rdf,toolbar,shortcut,search
#drush -y en-good
drush pm-enable  -y admin_menu,devel,module_filter,devel_generate

#Enable packaged features
drush pm-enable -y hcr_config

(cd themes/fb_foundation && compass compile)

#Enable FB-Foundation theme
drush pm-enable -y fb_foundation

#Modules to enable previously packaged by Max
drush  pm-enable -y admin_menu,admin_menu_toolbar,admin_views,advagg,coffee,ctools,views_content,page_manager,date,date_popup,diff,devel,entity,entitycache,entityreference,features,field_group,file_entity,jquery_update,libraries,link,linkit,media,media_wysiwyg,menu_block,menu_admin_per_menu,module_filter,multiform,panels,plupload,token,pathauto,redirect,scheduler,strongarm,views,views_bulk_operations,views_ui,wysiwyg,xmlsitemap

#Dump sql content
gzip -dc config.sql.gz | drush sqlc
gzip -dc content.sql.gz | drush sqlc

drush cc all
